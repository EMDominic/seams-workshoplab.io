---
slug: reuse
title: Reuse & Reusability
---

Make a library, module, package, whatever-the-term-is in your project's main language. *E.g.*, a [Python module](https://python-packaging.readthedocs.io/en/latest/) or [R package](http://r-pkgs.had.co.nz/).

The only functionality this package needs to have is a function `helloworld()`, taking no arguments, and printing the phrase "hello world" to the screen.

However!  Your package needs to take into account the factors that encourage reuseability discussed this morning.  So you should probably:

 - have it available via repository
 - have good documentation (for installation, for use, ...)
 - ...?